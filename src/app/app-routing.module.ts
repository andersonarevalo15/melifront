import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DetailComponent } from './detail/detail.component';
import { ResultComponent } from './result/result.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
  { path: '', component: SearchComponent},
  { path: 'items', component: ResultComponent},
  { path: 'items/:id', component: DetailComponent},
  { path: '**', component: ResultComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
