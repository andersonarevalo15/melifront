import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from './result/models/Item';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  loadingStatus$: Observable<boolean>;
  constructor(private loadingService: LoadingService) { }

  ngOnInit(): void {
    this.initProgress();
  }

  initProgress(): void {
    this.loadingStatus$ = this.loadingService.listenLoading();
  }
 
  title = 'MeliFront';
  
}
