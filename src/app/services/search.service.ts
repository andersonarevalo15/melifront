import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { resultMock } from '../app.mocks';
import { Result } from '../result/models/result';
import { Item } from '../result/models/Item';
import { ResultDetail } from '../result/models/ResultDetail';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  getListItems(filter: string): Observable<Result> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append("limit",'4');
    queryParams = queryParams.append("offset",'4');
    if(filter != undefined)
    queryParams = queryParams.append("q",filter);
    
    return environment.mock ?
    of(resultMock):
    this.http.get<Result>(environment.services.search.findbyfilter,{params: queryParams});
  }

  getDetailOfItem(id: string): Observable<ResultDetail> {
    return this.http.get<ResultDetail>(environment.services.search.detail+id);
  }
}
