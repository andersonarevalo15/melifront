import { Injectable } from '@angular/core';
import { off } from 'process';
import { Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  categories = new Subject<any[]>();
  constructor() { }

  setCategories(categories){
    console.log("desde util"+ categories);
    this.categories.next(categories);
  }

}
