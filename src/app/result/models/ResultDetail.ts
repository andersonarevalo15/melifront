import { Author } from "./Author";
import { Item } from "./Item";

export interface ResultDetail {
    author: Author;
    items: Item;
}