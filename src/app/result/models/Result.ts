import { Author } from "./Author";
import { Item } from "./Item";

export interface Result {
    author: Author;
    categories: string[];
    items: Item[];
}