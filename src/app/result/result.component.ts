import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { SearchService } from '../services/search.service';
import { UtilService } from '../services/util.service';
import { Item } from './models/Item';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.sass']
})
export class ResultComponent implements OnInit {
  items: Item[];
  categories: string[]= [];
  constructor(private searchService: SearchService,  
    private route: ActivatedRoute, 
    private router: Router,
    private utilservice: UtilService,
    private loadingService: LoadingService) { }

  ngOnInit(): void {
    let search = this.route.snapshot.queryParams.search;
    if(search != undefined)
    this.getListOfItems(search); 
  }

  catchResult(response) {
    this.items = response;
  }

  getListOfItems(filter?){
    this.loadingService.show();
    this.searchService.getListItems(filter).subscribe((response)=>{
      this.categories = response.categories;
      this.utilservice.setCategories(response.categories);
      this.items = response.items;
    });
  }

  viewdetail(id){
    this.router.navigate(['items',id]);
  }



}
