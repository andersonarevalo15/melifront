import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Result } from '../result/models/result';
import { Item } from '../result/models/Item';
import { SearchService } from '../services/search.service';
import { Router } from '@angular/router';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {
  @Output () valueResponse: EventEmitter<Item[]> = new EventEmitter();
  result: Result;
  constructor(private searchService: SearchService, 
    private utilService: UtilService, 
    private router: Router) {

  }

  ngOnInit(): void {
  }

  getListOfItems(filter?){
    this.searchService.getListItems(filter).subscribe((response)=>{
      this.utilService.setCategories(response.categories);
      this.router.navigate(['items'], {queryParams: {search: filter}});
      this.valueResponse.emit(response.items);
    });
  }
  
}
