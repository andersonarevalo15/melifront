import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.sass']
})
export class BreadcrumbComponent implements OnInit {
  categories: string[] =[];
  constructor(private serviceUtil: UtilService) {

    this.serviceUtil.categories.subscribe(categorieList => this.categories = categorieList);
   }
  

  ngOnInit(): void {
  }

}
