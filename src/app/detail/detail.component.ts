import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from '../result/models/Item';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {
  item: Item;
  constructor(private searchService: SearchService,  private route: ActivatedRoute) {
   }

  ngOnInit(): void {
    let id = this.route.snapshot.params['id'];
    if(id != undefined)
    this.getDetailOfItem(id); 
  }

  getDetailOfItem(id?: string){
    this.searchService.getDetailOfItem(id).subscribe((response)=>{
      this.item = response.items;
    });

  }


}
