import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from "rxjs/operators";
import { LoadingService } from './services/loading.service';
import Swal from 'sweetalert2';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor(private loadingService: LoadingService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loadingService.show();
    return next.handle(request)
        .pipe(
        tap((request)=>{
            this.loadingService.hide();
        }),
        catchError((error: HttpErrorResponse) => {
                let errorMsg = '';
                if (error.error instanceof ErrorEvent) {
                    this.loadingService.hide();
                    Swal.fire(
                        'Fallo del lado del cliente',
                        'Cerrar',
                        'error'
                    );
                    errorMsg = `Error: ${error.error.message}`;
                } else {
                    this.loadingService.hide();
                    Swal.fire(
                        'Fallo al realizar consumo al servidor',
                        'Cerrar',
                        'error'
                    );
                    errorMsg = `Error Code: ${request},  Message: ${error.message}`;
                }
                this.loadingService.hide();
                console.log(errorMsg);
                return throwError(errorMsg);
            })
        )
    }
}

