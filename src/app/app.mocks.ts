import { Result } from "./result/models/result";

export const resultMock: Result = {
    author: {
        name: "Prueba autor",
        lastname: "last name"
    },
    categories: [
        "Electronica, Audio y video",
        "iPod",
        "Reproductores",
        "iPod touch",
        "32 GB"
    ],
    items: [
        {
            id: "PruebaId1",
            title: "titulo prueba",
            price: {
                currency: "prueba",
                amount: 5000,
                decimals: 3
            },
            picture: "https://empresas.blogthinkbig.com/wp-content/uploads/2019/11/Imagen3-245003649.jpg?w=800",
            condition: "pruebaCondicion",
            free_shipping: true,
            state_name: "Capital Federal",
            sold_quantity: 0,
            description: ''
        },
        {
            id: "PruebaId2",
            title: "titulo prueba",
            price: {
                currency: "prueba",
                amount: 5000,
                decimals: 3
            },
            picture: "https://us.123rf.com/450wm/objowl/objowl1410/objowl141000017/33145448-una-imagen-fractal-resumen-digital-con-un-dise%C3%B1o-floral-de-estrella-psicod%C3%A9lico-en-azul-amarillo-ver.jpg?ver=6",
            condition: "pruebaCondicion",
            free_shipping: true,
            state_name: "Bogota",
            sold_quantity: 0,
            description: ''    
        },
        {
            id: "PruebaId3",
            title: "titulo prueba",
            price: {
                currency: "prueba",
                amount: 5000,
                decimals: 3
            },
            picture: "https://i.blogs.es/e1feab/google-fotos/450_1000.jpg",
            condition: "pruebaCondicion",
            free_shipping: true,
            state_name: "Soacha",
            sold_quantity: 0,
            description: ''    
        }, 
        {
            id: "PruebaId4",
            title: "titulo prueba",
            price: {
                currency: "prueba",
                amount: 5000,
                decimals: 3
            },
            picture: "https://i.blogs.es/594843/chrome/450_1000.jpg",
            condition: "pruebaCondicion",
            free_shipping: true, 
            state_name: "Ibague",
            sold_quantity: 0,
            description: ''   
        },
        {
            id: "PruebaId5",
            title: "titulo prueba",
            price: {
                currency: "prueba",
                amount: 5000,
                decimals: 3
            },
            picture: "https://www.astrobitacora.com/wp-content/uploads/2016/09/crab-nebula-esa-1024.jpg",
            condition: "pruebaCondicion",
            free_shipping: true, 
            state_name: "Buenos aires",
            sold_quantity: 0,
            description: ''    
        },         
    ]
}