const getUrl = window.location;
const HOST_API = getUrl.protocol + '//localhost:8080';
const url = `${HOST_API}/Meli/api/`;

export const environment = {
  mock: false,
  production: false,
  services: {
    search: {
      findbyfilter: `${url}items`,
      detail: `${url}item/`
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
